# Automação Web com CodeceptJS + Webdriver

Automação de Testes com Selenium WebDriver e Java. Projeto realizado no curso Trilha QA da Qazando.

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local para fins de desenvolvimento e teste.

Consulte **[Implantação](#-implanta%C3%A7%C3%A3o)** para saber como implantar o projeto.

## ⚙️ Executando os testes

Explicar como executar os testes automatizados para este sistema.

### Baixando o projeto

```
git clone https://github.com/erkmnasc/Qazando-Selenium-Webdriver-Basico.git
```

### 🔩 Analise os testes de ponta a ponta

Digito o código para iniciar os testes automatizados.

```
mvn test -Dtest=**/*RunCucumberTest cluecumber-report:reporting
```

Digito o código para abrir o resultado dos testes automatizados.

```
allure serve output
```

## 🛠️ Construído com

Mencione as ferramentas que você usou para criar seu projeto

* [Selenium WebDriver](https://www.selenium.dev/documentation/webdriver/)
* [Java Development Kit 8](https://www.oracle.com/br/java/technologies/javase-jdk8-doc-downloads.html)
* [Mavem](https://maven.apache.org/)
* [Junit](https://junit.org/junit5/)
* [Google WebDriver](https://chromedriver.chromium.org/downloads)
* [Firefox WebDriver](https://github.com/mozilla/geckodriver)
* 
* 

## ✒️ Autores

Mencione todos aqueles que ajudaram a levantar o projeto desde o seu início

* **QA** - *Automação* - *Documentação* - [Erick Nascimento]

GitHub - (https://github.com/erkmnasc)

Linkedin - (https://www.linkedin.com/in/erick-nascimento/)

GitShowCase - (https://www.gitshowcase.com/erkmnasc)


## 🎁 Expressões de gratidão

* Conte a outras pessoas sobre este projeto 📢;
* Convide alguém da equipe para uma cerveja 🍺;
* Um agradecimento publicamente 🫂;
* etc.


---
⌨️ com ❤️ por [Erick Nascimento](https://github.com/erkmnasc) 😊